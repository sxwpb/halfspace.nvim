local hscolors = require 'halfspace'.palette

return {
	normal = {
		a = { fg = hscolors.white, bg = hscolors.hsgray12 },
		b = { fg = hscolors.black, bg = hscolors.hsgray04 },
		c = { fg = hscolors.black, bg = hscolors.hsgray04 }
	},
	insert = {
		a = { fg = hscolors.turquoise_neg, bg = hscolors.hsgray12 },
		b = { fg = hscolors.turquoise, bg = hscolors.hsgray04 }
	},
	visual = {
		a = { fg = hscolors.purple_neg, bg = hscolors.hsgray12 },
		b = { fg = hscolors.purple, bg = hscolors.hsgray04 }
	},
	command = {
		a = { fg = hscolors.yellow_neg, bg = hscolors.hsgray12 },
		b = { fg = hscolors.yellow, bg = hscolors.hsgray04 }
	},
	replace = {
		a = { fg = hscolors.orange_neg, bg = hscolors.hsgray12 },
		b = { fg = hscolors.orange, bg = hscolors.hsgray04 }
	},
	inactive = {
		a = { fg = hscolors.black, bg = hscolors.hsgray04 },
		b = { fg = hscolors.black, bg = hscolors.hsgray04 },
		c = { fg = hscolors.black, bg = hscolors.hsgray04 }
	},
}
