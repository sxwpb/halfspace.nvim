local g = vim.g
local highlight = vim.api.nvim_set_hl

local none = 'NONE'

-- black and white
local black = '#000000'
local white = '#bababa'

-- grays
local hsgray00 = '#808080'
local hsgray01 = '#787878'
local hsgray02 = '#737373'
local hsgray03 = '#6e6e6e'
local hsgray04 = '#666666'
local hsgray05 = '#5e5e5e'
local hsgray06 = '#575757'
local hsgray07 = '#4f4f4f'
local hsgray08 = '#474747'
local hsgray09 = '#404040'
local hsgray10 = '#383838'
local hsgray11 = '#303030'
local hsgray12 = '#292929'
local hsgray13 = '#212121'
local hsgray14 = '#1c1c1c'
local hsgray15 = '#121212'

-- text colors
local khaki = '#282301'
local yellow = '#453500'
local orange = '#432700'
local coral = '#4e1c00'
local orchid = '#481420'
local lime = '#003f06'
local green = '#1f3f00'
local emerald = '#003824'
local blue = '#08215c'
local sky = '#002c44'
local turquoise = '#00383e'
local purple = '#480385'
local cranberry = '#3b0711'
local violet = '#3d094d'
local crimson = '#6b002e'
local red = '#53000e'

-- fill colors
local khaki_fill = '#716727'
local yellow_fill = '#766434'
local orange_fill = '#865c2a'
local coral_fill = '#8e563b'
local orchid_fill = '#90505b'
local lime_fill = '#467346'
local green_fill = '#51713d'
local emerald_fill = '#307557'
local blue_fill = '#4c6597'
local sky_fill = '#316c92'
local turquoise_fill = '#387177'
local purple_fill = '#6e5897'
local cranberry_fill = '#905156'
local violet_fill = '#7a5687'
local crimson_fill = '#8f505f'
local red_fill = '#915151'

-- background colors
local khaki_bg = '#84816d'
local yellow_bg = '#897f6c'
local orange_bg = '#8c7d6d'
local coral_bg = '#907b71'
local orchid_bg = '#91797c'
local lime_bg = '#768575'
local green_bg = '#798472'
local emerald_bg = '#71867b'
local blue_bg = '#778193'
local sky_bg = '#708391'
local turquoise_bg = '#6c8689'
local purple_bg = '#827d90'
local cranberry_bg = '#91797a'
local violet_bg = '#887b8d'
local crimson_bg = '#91797e'
local red_bg = '#927978'

-- misc colors
local difftxt = '#687795'
local diffaddtxt = '#6b7e5f'

-- text-neg colors
local khaki_neg = '#ccbc5a'
local yellow_neg = '#d9b658'
local orange_neg = '#f2a950'
local coral_neg = '#f2a580'
local orchid_neg = '#f29fac'
local lime_neg = '#89ce88'
local green_neg = '#99cb7c'
local emerald_neg = '#6ad2a3'
local blue_neg = '#9dbaf2'
local sky_neg = '#7ec3f2'
local turquoise_neg = '#79c9d2'
local purple_neg = '#c09fff'
local cranberry_neg = '#f2a0a5'
local violet_neg = '#d9a1ee'
local crimson_neg = '#f29eb1'
local red_neg = '#f2a19f'

-- bg
local bg = hsgray00

local M = {}

M.palette = {
	black = black,
	white = white,
	bg = bg,
	hsgray00 = hsgray00,
	hsgray01 = hsgray01,
	hsgray02 = hsgray02,
	hsgray03 = hsgray03,
	hsgray04 = hsgray04,
	hsgray05 = hsgray05,
	hsgray06 = hsgray06,
	hsgray07 = hsgray07,
	hsgray08 = hsgray08,
	hsgray09 = hsgray09,
	hsgray10 = hsgray10,
	hsgray11 = hsgray11,
	hsgray12 = hsgray12,
	hsgray13 = hsgray13,
	hsgray14 = hsgray14,
	hsgray15 = hsgray15,
	khaki = khaki,
	yellow = yellow,
	orange = orange,
	coral = coral,
	orchid = orchid,
	lime = lime,
	green = green,
	emerald = emerald,
	blue = blue,
	sky = sky,
	turquoise = turquoise,
	purple = purple,
	cranberry = cranberry,
	violet = violet,
	crimson = crimson,
	red = red,
	khaki_fill = khaki_fill,
	yellow_fill = yellow_fill,
	orange_fill = orange_fill,
	coral_fill = coral_fill,
	orchid_fill = orchid_fill,
	lime_fill = lime_fill,
	green_fill = green_fill,
	emerald_fill = emerald_fill,
	blue_fill = blue_fill,
	sky_fill = sky_fill,
	turquoise_fill = turquoise_fill,
	purple_fill = purple_fill,
	cranberry_fill = cranberry_fill,
	violet_fill = violet_fill,
	crimson_fill = crimson_fill,
	red_fill = red_fill,
	khaki_bg = khaki_bg,
	yellow_bg = yellow_bg,
	orange_bg = orange_bg,
	coral_bg = coral_bg,
	orchid_bg = orchid_bg,
	lime_bg = lime_bg,
	green_bg = green_bg,
	emerald_bg = emerald_bg,
	blue_bg = blue_bg,
	sky_bg = sky_bg,
	turquoise_bg = turquoise_bg,
	purple_bg = purple_bg,
	cranberry_bg = cranberry_bg,
	violet_bg = violet_bg,
	crimson_bg = crimson_bg,
	red_bg = red_bg,
	khaki_neg = khaki_neg,
	yellow_neg = yellow_neg,
	orange_neg = orange_neg,
	coral_neg = coral_neg,
	orchid_neg = orchid_neg,
	lime_neg = lime_neg,
	green_neg = green_neg,
	emerald_neg = emerald_neg,
	blue_neg = blue_neg,
	sky_neg = sky_neg,
	turquoise_neg = turquoise_neg,
	purple_neg = purple_neg,
	cranberry_neg = cranberry_neg,
	violet_neg = violet_neg,
	crimson_neg = crimson_neg,
	red_neg = red_neg,
	difftxt = difftxt,
	diffaddtxt = diffaddtxt,
}

M.style = function()
	-------------------------------------------------------------------------
	-- Custom styling groups
	-------------------------------------------------------------------------

	highlight(0, 'HalfspaceVisual', { bg = hsgray01 })
	highlight(0, 'HalfspaceWhite', { fg = white })
	highlight(0, 'Halfspacegray00', { fg = hsgray00 })
	highlight(0, 'Halfspacegray01', { fg = hsgray01 })
	highlight(0, 'Halfspacegray02', { fg = hsgray02 })
	highlight(0, 'Halfspacegray03', { fg = hsgray03 })
	highlight(0, 'Halfspacegray04', { fg = hsgray04 })
	highlight(0, 'Halfspacegray05', { fg = hsgray05 })
	highlight(0, 'Halfspacegray06', { fg = hsgray06 })
	highlight(0, 'Halfspacegray07', { fg = hsgray07 })
	highlight(0, 'Halfspacegray08', { fg = hsgray08 })
	highlight(0, 'Halfspacegray09', { fg = hsgray09 })
	highlight(0, 'Halfspacegray10', { fg = hsgray10 })
	highlight(0, 'Halfspacegray11', { fg = hsgray11 })
	highlight(0, 'Halfspacegray12', { fg = hsgray12 })
	highlight(0, 'Halfspacegray13', { fg = hsgray13 })
	highlight(0, 'Halfspacegray14', { fg = hsgray14 })
	highlight(0, 'Halfspacegray15', { fg = hsgray15 })
	highlight(0, 'HalfspaceKhaki', { fg = khaki })
	highlight(0, 'HalfspaceYellow', { fg = yellow })
	highlight(0, 'HalfspaceOrange', { fg = orange })
	highlight(0, 'HalfspaceCoral', { fg = coral })
	highlight(0, 'HalfspaceOrchid', { fg = orchid })
	highlight(0, 'HalfspaceLime', { fg = lime })
	highlight(0, 'HalfspaceGreen', { fg = green })
	highlight(0, 'HalfspaceEmerald', { fg = emerald })
	highlight(0, 'HalfspaceBlue', { fg = blue })
	highlight(0, 'HalfspaceSky', { fg = sky })
	highlight(0, 'HalfspaceTurquoise', { fg = turquoise })
	highlight(0, 'HalfspacePurple', { fg = purple })
	highlight(0, 'HalfspaceCranberry', { fg = cranberry })
	highlight(0, 'HalfspaceViolet', { fg = violet })
	highlight(0, 'HalfspaceCrimson', { fg = crimson })
	highlight(0, 'HalfspaceRed', { fg = red })
	-- Misc helpers
	highlight(0, 'HalfspaceUnderline', { underline = true })
	highlight(0, 'HalfspaceNoCombine', { nocombine = true })

	-------------------------------------------------------------------------
	-- Standard styling
	-------------------------------------------------------------------------

	-- colors used by inbuilt terminal
	g.terminal_color_0 = hsgray00
	g.terminal_color_1 = red
	g.terminal_color_2 = green
	g.terminal_color_3 = yellow
	g.terminal_color_4 = blue
	g.terminal_color_5 = violet
	g.terminal_color_6 = turquoise
	g.terminal_color_7 = black
	g.terminal_color_8 = hsgray08
	g.terminal_color_9 = crimson
	g.terminal_color_10 = emerald
	g.terminal_color_11 = khaki
	g.terminal_color_12 = sky
	g.terminal_color_13 = purple
	g.terminal_color_14 = lime
	g.terminal_color_15 = white

	-- Background and text
	highlight(0, 'Normal', { bg = bg, fg = black })

	-- Color of mode text, -- INSERT --
	highlight(0, 'ModeMsg', { fg = black })

	-- Comments
	highlight(0, 'Comment', { link = 'Halfspacegray06' })

	-- Functions
	highlight(0, 'Function', { link = 'HalfspaceSky' })

	-- Strings
	highlight(0, 'String', { link = 'HalfspaceKhaki' })

	-- Booleans
	highlight(0, 'Boolean', { link = 'HalfspaceCranberry' })

	-- Identifiers
	highlight(0, 'Identifier', { link = 'HalfspaceTurquoise' })

	-- Color of titles
	highlight(0, 'Title', { fg = orange })

	-- Color of directories
	highlight(0, 'Directory', { fg = blue })

	-- const, static
	highlight(0, 'StorageClass', { link = 'HalfspaceCoral' })

	-- void, intptr_t
	highlight(0, 'Type', { fg = emerald })

	-- Numbers
	highlight(0, 'Constant', { link = 'HalfspaceOrange' })

	-- Character constants
	highlight(0, 'Character', { link = 'HalfspacePurple' })

	-- Exceptions
	highlight(0, 'Exception', { link = 'HalfspaceCrimson' })

	-- ifdef/endif
	highlight(0, 'PreProc', { link = 'HalfspaceCranberry' })

	-- case in switch statement
	highlight(0, 'Label', { link = 'HalfspaceTurquoise' })

	-- end-of-line '$', end-of-file '~'
	highlight(0, 'NonText', { fg = hsgray04 })

	-- sizeof
	highlight(0, 'Operator', { link = 'HalfspaceCranberry' })

	-- for, while
	highlight(0, 'Repeat', { link = 'HalfspaceViolet' })

	-- Search
	highlight(0, 'Search', { bg = hsgray01, fg = black })
	highlight(0, 'CurSearch', { bg = coral, fg = black })
	highlight(0, 'IncSearch', { bg = yellow, fg = black })

	-- '\n' sequences
	highlight(0, 'Special', { link = 'HalfspaceCranberry' })

	-- if, else
	highlight(0, 'Statement', { fg = violet })

	-- struct, union, enum, typedef
	highlight(0, 'Structure', { link = 'HalfspaceBlue' })

	-- Status, split and tab lines
	highlight(0, 'StatusLine', { bg = hsgray04, fg = black })
	highlight(0, 'StatusLineNC', { bg = hsgray11, fg = black })
	highlight(0, 'Tabline', { bg = hsgray04, fg = black })
	highlight(0, 'TablineSel', { bg = hsgray00, fg = black })
	highlight(0, 'TablineFill', { bg = hsgray07, fg = hsgray00 })
	highlight(0, 'StatusLineTerm', { bg = hsgray11, fg = hsgray00 })
	highlight(0, 'StatusLineTermNC', { bg = hsgray11, fg = black })
	highlight(0, 'VertSplit', { bg = none, fg = hsgray11 })

	-- Visual selection
	highlight(0, 'Visual', { bg = hsgray01 })

	-- Errors, warnings and whitespace-eol
	highlight(0, 'Error', { bg = bg, fg = red })
	highlight(0, 'ErrorMsg', { bg = bg, fg = red })
	highlight(0, 'WarningMsg', { bg = bg, fg = orange })

	-- Auto-text-completion menu
	highlight(0, 'Pmenu', { bg = hsgray14, fg = white })
	highlight(0, 'PmenuSel', { bg = hsgray10, fg = white })
	highlight(0, 'PmenuSbar', { bg = hsgray13 })
	highlight(0, 'PmenuThumb', { bg = hsgray08 })
	highlight(0, 'WildMenu', { bg = hsgray14, fg = white })

	-- Spelling errors
	highlight(0, 'SpellBad', { bg = none, fg = red, underline = true, sp = red })
	highlight(0, 'SpellCap', { bg = none, fg = blue, underline = true, sp = blue })
	highlight(0, 'SpellRare', { bg = none, fg = yellow, underline = true, sp = yellow })
	highlight(0, 'SpellLocal', { bg = none, fg = sky, underline = true, sp = sky })

	-- Misc
	highlight(0, 'Question', { fg = lime })
	highlight(0, 'MoreMsg', { fg = red })
	highlight(0, 'LineNr', { bg = bg, fg = hsgray04 })
	highlight(0, 'Cursor', { fg = bg, bg = black })
	highlight(0, 'lCursor', { fg = bg, bg = black })
	highlight(0, 'CursorLineNr', { bg = bg, fg = hsgray12 })
	highlight(0, 'CursorColumn', { bg = hsgray14 })
	highlight(0, 'CursorLine', { bg = none, underline = true, sp = hsgray09 })
	highlight(0, 'Folded', { bg = none, undercurl = true, fg = black, sp = hsgray05 })
	highlight(0, 'FoldColumn', { bg = hsgray04, fg = black })
	highlight(0, 'SignColumn', { bg = bg, fg = lime })
	highlight(0, 'Todo', { bg = yellow_bg, fg = yellow })
	highlight(0, 'SpecialKey', { bg = bg, fg = sky })
	highlight(0, 'MatchParen', { link = 'HalfspaceVisual' })
	highlight(0, 'Ignore', { link = 'HalfspaceSky' })
	highlight(0, 'Underlined', { fg = emerald })
	highlight(0, 'Delimiter', { fg = black })
	highlight(0, 'QuickFixLine', { bg = hsgray01 })
	highlight(0, 'qfFileName', { link = 'HalfspaceEmerald' })
	highlight(0, 'qfLineNr', { fg = orange })

	-- Color column (after line 80)
	highlight(0, 'ColorColumn', { bg = hsgray02 })

	-- Conceal color
	highlight(0, 'Conceal', { bg = none, fg = black })

	-- nvim -d
	highlight(0, 'DiffAdd', { bg = green_bg })
	highlight(0, 'DiffChange', { bg = blue_bg })
	highlight(0, 'DiffDelete', { bg = red_bg, fg = red })
	highlight(0, 'DiffText', { bg = difftxt })

	-------------------------------------------------------------------------
	-- Neovim standard styling
	-------------------------------------------------------------------------

	highlight(0, 'Whitespace', { link = 'Halfspacegray03' })
	highlight(0, 'TermCursor', { bg = black, fg = black })
	highlight(0, 'NormalFloat', { bg = bg, fg = black })
	highlight(0, 'FloatBorder', { bg = bg, fg = black })
	highlight(0, 'FloatTitle', { bg = bg, fg = black })
	highlight(0, 'WinBar', { bg = hsgray04, fg = black })
	highlight(0, 'WinBarNC', { bg = hsgray04, fg = black })
	highlight(0, 'WinSeparator', { link = 'VertSplit' })

	-- Neovim check-health
	highlight(0, 'healthSuccess', { link = 'DiffAdd' })
	highlight(0, 'healthError', { link = 'DiffDelete' })
	highlight(0, 'healthHeadingChar', { link = 'HalfspaceBlue' })
	highlight(0, 'helpHeader', { link = 'HalfspaceTurquoise' })

	-- Neovim Tree-sitter
	highlight(0, '@attribute', { link = 'HalfspaceSky' })
	highlight(0, '@comment.error', { link = 'HalfspaceRed' })
	highlight(0, '@comment.note', { link = 'Halfspacegray06' })
	highlight(0, '@comment.ok', { link = 'HalfspaceGreen' })
	highlight(0, '@comment.todo', { link = 'Todo' })
	highlight(0, '@comment.warning', { link = 'HalfspaceYellow' })
	highlight(0, '@constant', { link = 'HalfspaceTurquoise' })
	highlight(0, '@constant.builtin', { link = 'HalfspaceGreen' })
	highlight(0, '@constant.macro', { link = 'HalfspaceViolet' })
	highlight(0, '@constructor', { link = 'HalfspaceEmerald' })
	highlight(0, '@diff.delta', { link = 'DiffChange' })
	highlight(0, '@diff.minus', { link = 'DiffDelete' })
	highlight(0, '@diff.plus', { link = 'DiffAdd' })
	highlight(0, '@function.builtin', { link = 'Function' })
	highlight(0, '@function.call', { link = 'Function' })
	highlight(0, '@function.macro', { link = 'HalfspaceTurquoise' })
	highlight(0, '@function.method', { link = 'Function' })
	highlight(0, '@function.method.call', { link = 'Function' })
	highlight(0, '@keyword.conditional', { link = 'Conditional' })
	highlight(0, '@keyword.directive', { link = 'PreProc' })
	highlight(0, '@keyword.directive.define', { link = 'Define' })
	highlight(0, '@keyword.exception', { link = 'HalfspaceViolet' })
	highlight(0, '@keyword.import', { link = 'Include' })
	highlight(0, '@keyword.operator', { link = 'HalfspaceViolet' })
	highlight(0, '@keyword.repeat', { link = 'Repeat' })
	highlight(0, '@keyword.storage', { link = 'StorageClass' })
	highlight(0, '@markup.environment', { link = 'HalfspaceCranberry' })
	highlight(0, '@markup.environment.name', { link = 'HalfspaceEmerald' })
	highlight(0, '@markup.heading', { link = 'HalfspaceViolet' })
	highlight(0, '@markup.italic', { fg = orchid, italic = true })
	highlight(0, '@markup.link.label', { link = 'HalfspaceGreen' })
	highlight(0, '@markup.link.url', { link = 'HalfspacePurple' })
	highlight(0, '@markup.list', { link = 'HalfspaceCranberry' })
	highlight(0, '@markup.list.checked', { link = 'HalfspaceTurquoise' })
	highlight(0, '@markup.list.unchecked', { link = 'HalfspaceBlue' })
	highlight(0, '@markup.math', { link = 'HalfspaceCranberry' })
	highlight(0, '@markup.quote', { link = 'Halfspacegray06' })
	highlight(0, '@markup.raw', { link = 'String' })
	highlight(0, '@markup.strikethrough', { strikethrough = true })
	highlight(0, '@markup.strong', { link = 'HalfspaceOrchid' })
	highlight(0, '@markup.underline', { underline = true })
	highlight(0, '@module', { link = 'HalfspaceTurquoise' })
	highlight(0, '@module.builtin', { link = 'HalfspaceGreen' })
	highlight(0, '@none', {})
	highlight(0, '@parameter.builtin', { link = 'HalfspaceOrchid' })
	highlight(0, '@property', { link = 'HalfspaceTurquoise' })
	highlight(0, '@string.regexp', { link = 'HalfspaceTurquoise' })
	highlight(0, '@string.special.symbol', { link = 'HalfspacePurple' })
	highlight(0, '@string.special.url', { link = 'HalfspacePurple' })
	highlight(0, '@tag', { link = 'HalfspaceBlue' })
	highlight(0, '@tag.attribute', { link = 'HalfspaceTurquoise' })
	highlight(0, '@tag.builtin', { link = 'HalfspaceBlue' })
	highlight(0, '@tag.delimiter', { link = 'HalfspaceGreen' })
	highlight(0, '@type.builtin', { link = 'HalfspaceEmerald' })
	highlight(0, '@type.qualifier', { link = 'HalfspaceViolet' })
	highlight(0, '@variable', { fg = black })
	highlight(0, '@variable.builtin', { link = 'HalfspaceGreen' })
	highlight(0, '@variable.member', { link = 'HalfspaceTurquoise' })
	highlight(0, '@variable.parameter', { link = 'HalfspaceOrchid' })
	-- Language specific Tree-sitter overrides.
	highlight(0, '@attribute.zig', { link = 'HalfspaceViolet' })
	highlight(0, '@function.macro.vim', { link = 'HalfspaceSky' })
	highlight(0, '@keyword.gitcommit', { link = 'HalfspaceSky' })
	highlight(0, '@keyword.import.bash', { link = '@keyword' })
	highlight(0, '@keyword.import.rust', { link = '@keyword' })
	highlight(0, '@keyword.storage.rust', { link = 'HalfspaceViolet' })
	highlight(0, '@markup.heading.1.markdown', { link = 'HalfspaceEmerald' })
	highlight(0, '@markup.heading.1.vimdoc', { link = 'HalfspaceBlue' })
	highlight(0, '@markup.heading.2.markdown', { link = 'HalfspaceTurquoise' })
	highlight(0, '@markup.heading.2.vimdoc', { link = 'HalfspaceBlue' })
	highlight(0, '@markup.heading.3.markdown', { link = 'HalfspaceTurquoise' })
	highlight(0, '@markup.heading.4.markdown', { link = 'HalfspaceOrange' })
	highlight(0, '@markup.heading.5.markdown', { link = 'HalfspaceSky' })
	highlight(0, '@markup.heading.6.markdown', { link = 'HalfspaceViolet' })
	highlight(0, '@markup.heading.help', { link = 'HalfspaceSky' })
	highlight(0, '@markup.heading.markdown', { link = 'HalfspaceSky' })
	highlight(0, '@markup.link.gitcommit', { link = 'HalfspaceBlue' })
	highlight(0, '@markup.link.markdown_inline', { fg = black })
	highlight(0, '@markup.link.url.gitcommit', { link = 'HalfspaceEmerald' })
	highlight(0, '@markup.link.url.astro', { link = 'HalfspaceViolet' })
	highlight(0, '@markup.link.url.html', { link = 'HalfspaceViolet' })
	highlight(0, '@markup.link.url.svelte', { link = 'HalfspaceViolet' })
	highlight(0, '@markup.link.url.vue', { link = 'HalfspaceViolet' })
	highlight(0, '@markup.raw.block.vimdoc', { link = 'HalfspaceGreen' })
	highlight(0, '@markup.raw.vimdoc', { link = 'HalfspaceOrchid' })
	highlight(0, '@punctuation.delimiter.astro', { link = 'HalfspaceCranberry' })
	highlight(0, '@punctuation.delimiter.css', { link = 'HalfspaceCranberry' })
	highlight(0, '@punctuation.delimiter.rust', { link = 'HalfspaceCranberry' })
	highlight(0, '@punctuation.delimiter.scss', { link = 'HalfspaceCranberry' })
	highlight(0, '@punctuation.delimiter.yaml', { link = 'HalfspaceCranberry' })
	highlight(0, '@variable.builtin.vim', { link = 'HalfspaceEmerald' })
	highlight(0, '@variable.member.yaml', { link = 'HalfspaceBlue' })
	highlight(0, '@variable.parameter.bash', { link = 'HalfspaceTurquoise' })
	highlight(0, '@variable.scss', { link = 'HalfspaceTurquoise' })
	highlight(0, '@variable.vim', { link = 'HalfspaceTurquoise' })
	-- Neovim LEGACY Tree-sitter (Neovim 0.8, 0.9)
	highlight(0, '@conditional', { link = '@keyword.conditional' })
	highlight(0, '@define', { link = '@keyword.directive.define' })
	highlight(0, '@error', { link = 'HalfspaceRed' })
	highlight(0, '@exception', { link = '@keyword.exception' })
	highlight(0, '@field', { link = '@variable.member' })
	highlight(0, '@include', { link = '@keyword.import' })
	highlight(0, '@method', { link = '@function.method' })
	highlight(0, '@namespace', { link = '@module' })
	highlight(0, '@namespace.builtin', { link = '@module.builtin' })
	highlight(0, '@parameter', { link = '@variable.parameter' })
	highlight(0, '@preproc', { link = '@keyword.directive' })
	highlight(0, '@punctuation.special', { link = '@markup.list' })
	highlight(0, '@repeat', { link = '@keyword.repeat' })
	highlight(0, '@string.regex', { link = '@string.regexp' })
	highlight(0, '@storageclass', { link = '@keyword.storage' })
	highlight(0, '@symbol', { link = '@string.special.symbol' })
	highlight(0, '@text.danger', { link = '@comment.error' })
	highlight(0, '@text.diff.add', { link = '@diff.plus' })
	highlight(0, '@text.diff.change', { link = '@diff.delta' })
	highlight(0, '@text.diff.delete', { link = '@diff.minus' })
	highlight(0, '@text.emphasis', { link = '@markup.italic' })
	highlight(0, '@text.environment', { link = '@markup.environment' })
	highlight(0, '@text.environment.name', { link = '@markup.environment.name' })
	highlight(0, '@text.literal', { link = '@markup.raw' })
	highlight(0, '@text.math', { link = '@markup.math' })
	highlight(0, '@text.note', { link = '@comment.note' })
	highlight(0, '@text.reference', { link = '@markup.link.label' })
	highlight(0, '@text.strike', { link = '@markup.strikethrough' })
	highlight(0, '@text.strong', { link = '@markup.strong' })
	highlight(0, '@text.title', { link = '@markup.heading' })
	highlight(0, '@text.todo', { link = '@comment.todo' })
	highlight(0, '@text.underline', { link = '@markup.underline' })
	highlight(0, '@text.uri', { link = '@markup.link.url' })
	highlight(0, '@text.warning', { link = '@comment.warning' })
	-- Language specific LEGACY Tree-sitter overrides (Neovim 0.8, 0.9).
	highlight(0, '@field.yaml', { link = '@variable.member.yaml' })
	highlight(0, '@include.rust', { link = '@keyword.import.rust' })
	highlight(0, '@parameter.bash', { link = '@variable.parameter.bash' })
	highlight(0, '@storageclass.rust', { link = '@keyword.storage.rust' })
	highlight(0, '@text.literal.block.vimdoc', { link = '@markup.raw.block.vimdoc' })
	highlight(0, '@text.literal.vimdoc', { link = '@markup.raw.vimdoc' })
	highlight(0, '@text.reference.gitcommit', { link = '@markup.link.gitcommit' })
	highlight(0, '@text.reference.markdown_inline', { link = '@markup.link.label' })
	highlight(0, '@text.title.1.markdown', { link = '@markup.heading.1.markdown' })
	highlight(0, '@text.title.1.marker.markdown', { link = '@markup.list' })
	highlight(0, '@text.title.1.vimdoc', { link = '@markup.heading.1.vimdoc' })
	highlight(0, '@text.title.2.markdown', { link = '@markup.heading.2.markdown' })
	highlight(0, '@text.title.2.marker.markdown', { link = '@markup.list' })
	highlight(0, '@text.title.2.vimdoc', { link = '@markup.heading.2.vimdoc' })
	highlight(0, '@text.title.3.markdown', { link = '@markup.heading.3.markdown' })
	highlight(0, '@text.title.3.marker.markdown', { link = '@markup.list' })
	highlight(0, '@text.title.4.markdown', { link = '@markup.heading.4.markdown' })
	highlight(0, '@text.title.4.marker.markdown', { link = '@markup.list' })
	highlight(0, '@text.title.5.markdown', { link = '@markup.heading.5.markdown' })
	highlight(0, '@text.title.5.marker.markdown', { link = '@markup.list' })
	highlight(0, '@text.title.6.markdown', { link = '@markup.heading.6.markdown' })
	highlight(0, '@text.title.6.marker.markdown', { link = '@markup.list' })
	highlight(0, '@text.title.help', { link = '@markup.heading.help' })
	highlight(0, '@text.title.markdown', { link = '@markup.heading.markdown' })
	highlight(0, '@text.uri.astro', { link = '@markup.link.url.astro' })
	highlight(0, '@text.uri.gitcommit', { link = '@markup.link.url.gitcommit' })
	highlight(0, '@text.uri.html', { link = '@markup.link.url.html' })
	highlight(0, '@text.uri.svelte', { link = '@markup.link.url.svelte' })
	highlight(0, '@text.uri.vue', { link = '@markup.link.url.vue' })

	-- Neovim LSP semantic highlights.
	highlight(0, '@lsp.mod.deprecated', { link = '@constant' })
	highlight(0, '@lsp.mod.readonly', { link = '@constant' })
	highlight(0, '@lsp.mod.typeHint', { link = '@type' })
	highlight(0, '@lsp.type.boolean', { link = '@boolean' })
	highlight(0, '@lsp.type.builtinConstant', { link = '@constant.builtin' })
	highlight(0, '@lsp.type.builtinType', { link = '@type' })
	highlight(0, '@lsp.type.class', { link = '@type' })
	highlight(0, '@lsp.type.enum', { link = '@type' })
	highlight(0, '@lsp.type.enumMember', { link = '@property' })
	highlight(0, '@lsp.type.escapeSequence', { link = '@string.escape' })
	highlight(0, '@lsp.type.formatSpecifier', { link = '@punctuation.special' })
	highlight(0, '@lsp.type.generic', { link = '@variable' })
	highlight(0, '@lsp.type.interface', { link = '@type' })
	highlight(0, '@lsp.type.keyword', { link = '@keyword' })
	highlight(0, '@lsp.type.lifetime', { link = '@storageclass' })
	highlight(0, '@lsp.type.magicFunction', { link = '@function' })
	highlight(0, '@lsp.type.namespace', { link = '@namespace' })
	highlight(0, '@lsp.type.number', { link = '@number' })
	highlight(0, '@lsp.type.operator', { link = '@operator' })
	highlight(0, '@lsp.type.parameter', { link = '@parameter' })
	highlight(0, '@lsp.type.property', { link = '@property' })
	highlight(0, '@lsp.type.selfKeyword', { link = '@variable.builtin' })
	highlight(0, '@lsp.type.selfParameter', { link = '@variable.builtin' })
	highlight(0, '@lsp.type.string', { link = '@string' })
	highlight(0, '@lsp.type.struct', { link = '@type' })
	highlight(0, '@lsp.type.typeAlias', { link = '@type.definition' })
	highlight(0, '@lsp.type.unresolvedReference', { link = '@error' })
	highlight(0, '@lsp.type.variable', { link = '@variable' })
	highlight(0, '@lsp.typemod.class.defaultLibrary', { link = '@type' })
	highlight(0, '@lsp.typemod.enum.defaultLibrary', { link = '@type' })
	highlight(0, '@lsp.typemod.enumMember.defaultLibrary', { link = '@constant.builtin' })
	highlight(0, '@lsp.typemod.function.defaultLibrary', { link = '@function' })
	highlight(0, '@lsp.typemod.keyword.async', { link = '@keyword' })
	highlight(0, '@lsp.typemod.keyword.injected', { link = '@keyword' })
	highlight(0, '@lsp.typemod.method.defaultLibrary', { link = '@function' })
	highlight(0, '@lsp.typemod.operator.injected', { link = '@operator' })
	highlight(0, '@lsp.typemod.string.injected', { link = '@string' })
	highlight(0, '@lsp.typemod.struct.defaultLibrary', { link = '@type' })
	highlight(0, '@lsp.typemod.variable.callable', { link = '@function' })
	highlight(0, '@lsp.typemod.variable.defaultLibrary', { link = '@variable.builtin' })
	highlight(0, '@lsp.typemod.variable.global', { link = '@constant' })
	highlight(0, '@lsp.typemod.variable.injected', { link = '@variable' })
	highlight(0, '@lsp.typemod.variable.readonly', { link = '@constant' })
	highlight(0, '@lsp.typemod.variable.static', { link = '@constant' })
	-- Language specific LSP semantic overrides.
	highlight(0, '@lsp.type.macro.rust', { link = '@function' })
	highlight(0, '@lsp.type.parameter.dockerfile', { link = '@property' })
	highlight(0, '@lsp.type.variable.dockerfile', { link = '@function' })

	-- Neovim Diagnostic
	highlight(0, 'DiagnosticError', { link = 'HalfspaceRed' })
	highlight(0, 'DiagnosticWarn', { link = 'HalfspaceYellow' })
	highlight(0, 'DiagnosticInfo', { link = 'HalfspaceSky' })
	highlight(0, 'DiagnosticHint', { link = 'HalfspaceBlack' })
	highlight(0, 'DiagnosticOk', { link = 'HalfspaceEmerald' })
	highlight(0, 'DiagnosticUnderlineError', { bg = red_bg, underline = true, sp = red })
	highlight(0, 'DiagnosticUnderlineWarn', { bg = yellow_bg, underline = true, sp = yellow })
	highlight(0, 'DiagnosticUnderlineInfo', { bg = sky_bg, underline = true, sp = sky })
	highlight(0, 'DiagnosticUnderlineHint', { underline = true, sp = black })
	highlight(0, 'DiagnosticUnderlineOk', { underline = true, sp = emerald })
	highlight(0, 'DiagnosticVirtualTextError', { bg = red_bg, fg = red })
	highlight(0, 'DiagnosticVirtualTextWarn', { bg = yellow_bg, fg = yellow })
	highlight(0, 'DiagnosticVirtualTextInfo', { bg = blue_bg, fg = sky })
	highlight(0, 'DiagnosticVirtualTextHint', { bg = hsgray02, fg = black })
	highlight(0, 'DiagnosticVirtualTextOk', { bg = hsgray04, fg = emerald })
	highlight(0, 'DiagnosticSignError', { link = 'HalfspaceRed' })
	highlight(0, 'DiagnosticSignWarn', { link = 'HalfspaceYellow' })
	highlight(0, 'DiagnosticSignInfo', { link = 'HalfspaceSky' })
	highlight(0, 'DiagnosticSignHint', { link = 'HalfspaceBlack' })
	highlight(0, 'DiagnosticSignOk', { link = 'HalfspaceEmerald' })
	highlight(0, 'DiagnosticFloatingError', { link = 'HalfspaceRed' })
	highlight(0, 'DiagnosticFloatingWarn', { link = 'HalfspaceYellow' })
	highlight(0, 'DiagnosticFloatingInfo', { link = 'HalfspaceSky' })
	highlight(0, 'DiagnosticFloatingHint', { link = 'HalfspaceBlack' })
	highlight(0, 'DiagnosticFloatingOk', { link = 'HalfspaceEmerald' })

	-- Neovim LSP
	highlight(0, 'LspCodeLens', { link = 'Halfspacegray04' })
	highlight(0, 'LspCodeLensSeparator', { link = 'Halfspacegray04' })
	highlight(0, 'LspInfoBorder', { link = 'FloatBorder' })
	highlight(0, 'LspInlayHint', { bg = hsgray03, fg = black })
	highlight(0, 'LspReferenceText', { link = 'HalfspaceVisual' })
	highlight(0, 'LspReferenceRead', { link = 'HalfspaceVisual' })
	highlight(0, 'LspReferenceWrite', { link = 'HalfspaceVisual' })
	highlight(0, 'LspSignatureActiveParameter', { link = 'HalfspaceVisual' })

	-------------------------------------------------------------------------
	-- plugin styling
	-------------------------------------------------------------------------

	-- fern.vim
	highlight(0, 'FernBranchSymbol', { link = 'Halfspacegray04' })
	highlight(0, 'FernLeafSymbol', { link = 'HalfspaceBlue' })
	highlight(0, 'FernLeaderSymbol', { fg = black })
	highlight(0, 'FernBranchText', { link = 'HalfspaceBlue' })
	highlight(0, 'FernMarkedLine', { link = 'HalfspaceVisual' })
	highlight(0, 'FernMarkedText', { link = 'HalfspaceCrimson' })
	highlight(0, 'FernRootSymbol', { link = 'HalfspacePurple' })
	highlight(0, 'FernRootText', { link = 'HalfspacePurple' })

	-- NvimTree
	highlight(0, 'NvimTreeFolderIcon', { fg = blue_fill })
	highlight(0, 'NvimTreeFolderName', { fg = blue })
	highlight(0, 'NvimTreeIndentMarker', { fg = black })
	highlight(0, 'NvimTreeOpenedFolderName', { link = 'HalfspaceBlue' })
	highlight(0, 'NvimTreeRootFolder', { link = 'HalfspacePurple' })
	highlight(0, 'NvimTreeSpecialFile', { link = 'HalfspaceKhaki' })
	highlight(0, 'NvimTreeWindowPicker', { link = 'DiffChange' })
	highlight(0, 'NvimTreeCursorLine', { bg = hsgray03 })
	highlight(0, 'NvimTreeExecFile', { fg = green })
	highlight(0, 'NvimTreeImageFile', { fg = violet })
	highlight(0, 'NvimTreeOpenedFile', { fg = yellow })
	highlight(0, 'NvimTreeSymlink', { fg = turquoise })
	highlight(0, 'NvimTreeGitDirty', { fg = yellow })
	highlight(0, 'NvimTreeGitStaged', { fg = green })
	highlight(0, 'NvimTreeGitDeleted', { fg = red })
	highlight(0, 'NvimTreeGitMerge', { fg = green })
	highlight(0, 'NvimTreeGitRenamed', { fg = hsgray08 })
	highlight(0, 'NvimTreeGitNew', { fg = green })
	highlight(0, 'NvimTreeGitIgnored', { fg = hsgray08 })

	-- oil.nvim
	highlight(0, 'OilDir', { fg = blue })
	highlight(0, 'OilDirIcon', { fg = blue_fill })

	-- Neo-tree
	highlight(0, 'NeoTreeCursorLine', { bg = hsgray03 })
	highlight(0, 'NeoTreeDimText', { fg = black })
	highlight(0, 'NeoTreeDirectoryIcon', { fg = blue_fill })
	highlight(0, 'NeoTreeDotfile', { link = 'Halfspacegray09' })
	highlight(0, 'NeoTreeFilterTerm', { link = 'HalfspaceBlue' })
	highlight(0, 'NeoTreeGitAdded', { link = 'HalfspaceGreen' })
	highlight(0, 'NeoTreeGitConflict', { link = 'HalfspaceCrimson' })
	highlight(0, 'NeoTreeGitModified', { link = 'HalfspaceTurquoise' })
	highlight(0, 'NeoTreeGitUntracked', { link = 'HalfspaceOrchid' })
	highlight(0, 'NeoTreeIndentMarker', { fg = black })
	highlight(0, 'NeoTreeMessage', { fg = black })
	highlight(0, 'NeoTreeModified', { link = 'HalfspaceYellow' })
	highlight(0, 'NeoTreeRootName', { link = 'HalfspacePurple' })
	highlight(0, 'NeoTreeTitleBar', { bg = hsgray11, fg = white })

	-- Telescope
	highlight(0, 'TelescopeBorder', { link = 'FloatBorder' })
	highlight(0, 'TelescopeMatching', { link = 'HalfspaceCoral' })
	highlight(0, 'TelescopeMultiIcon', { link = 'HalfspaceCrimson' })
	highlight(0, 'TelescopeMultiSelection', { link = 'HalfspaceEmerald' })
	highlight(0, 'TelescopeNormal', { fg = black })
	highlight(0, 'TelescopePreviewDate', { link = 'Halfspacegray04' })
	highlight(0, 'TelescopePreviewGroup', { link = 'Halfspacegray04' })
	highlight(0, 'TelescopePreviewLink', { link = 'HalfspaceTurquoise' })
	highlight(0, 'TelescopePreviewMatch', { link = 'HalfspaceVisual' })
	highlight(0, 'TelescopePreviewRead', { link = 'HalfspaceOrange' })
	highlight(0, 'TelescopePreviewSize', { link = 'HalfspaceEmerald' })
	highlight(0, 'TelescopePreviewUser', { link = 'Halfspacegray04' })
	highlight(0, 'TelescopePromptPrefix', { link = 'HalfspaceBlue' })
	highlight(0, 'TelescopeResultsDiffAdd', { link = 'HalfspaceGreen' })
	highlight(0, 'TelescopeResultsDiffChange', { link = 'HalfspaceRed' })
	highlight(0, 'TelescopeResultsDiffDelete', { bg = red_bg, fg = red_fill })
	highlight(0, 'TelescopeResultsSpecialComment', { link = 'Halfspacegray04' })
	highlight(0, 'TelescopeSelectionCaret', { link = 'HalfspaceCrimson' })
	highlight(0, 'TelescopeTitle', { link = 'FloatTitle' })
	highlight(0, 'TelescopeSelection', { bg = hsgray02, fg = black })

	-- gitsigns.nvim
	highlight(0, 'GitSignsAdd', { link = 'HalfspaceEmerald' })
	highlight(0, 'GitSignsChange', { link = 'HalfspaceSky' })
	highlight(0, 'GitSignsChangeDelete', { link = 'HalfspaceCoral' })
	highlight(0, 'GitSignsDelete', { link = 'HalfspaceRed' })
	highlight(0, 'GitSignsUntracked', { link = 'Halfspacegray04' })
	-- line highlights
	highlight(0, 'GitSignsAddLn', { link = 'DiffAdd' })
	highlight(0, 'GitSignsChangeLn', { link = 'DiffChange' })
	-- word diff
	highlight(0, 'GitSignsAddLnInline', { bg = diffaddtxt })
	highlight(0, 'GitSignsChangeLnInline', { link = 'DiffText' })
	-- word diff in preview
	highlight(0, 'GitSignsAddInline', { link = 'DiffAdd' })
	highlight(0, 'GitSignsChangeInline', { link = 'DiffChange' })
	highlight(0, 'GitSignsDeleteInline', { link = 'DiffDelete' })
	-- misc
	highlight(0, 'GitSignsAddPreview', { link = 'DiffAdd' })
	highlight(0, 'GitSignsDeletePreview', { link = 'DiffDelete' })
	highlight(0, 'GitSignsDeleteVirtLn', { link = 'DiffDelete' })

	-- Bufferline
	highlight(0, 'BufferLineTabSelected', { fg = blue })
	highlight(0, 'BufferLineIndicatorSelected', { fg = blue })

	-- nvim-cmp
	highlight(0, 'CmpItemAbbr', { fg = black })
	highlight(0, 'CmpItemAbbrMatch', { link = 'HalfspaceYellow' })
	highlight(0, 'CmpItemAbbrMatchFuzzy', { link = 'HalfspaceCoral' })
	highlight(0, 'CmpItemAbbrDeprecated', { fg = hsgray05, bg = 'NONE', strikethrough = true })
	highlight(0, 'CmpItemKind', { link = 'HalfspaceBlack' })
	highlight(0, 'CmpItemKindClass', { link = 'HalfspaceEmerald' })
	highlight(0, 'CmpItemKindColor', { link = 'HalfspaceTurquoise' })
	highlight(0, 'CmpItemKindConstant', { link = 'HalfspacePurple' })
	highlight(0, 'CmpItemKindConstructor', { link = 'HalfspaceSky' })
	highlight(0, 'CmpItemKindEnum', { link = 'HalfspaceViolet' })
	highlight(0, 'CmpItemKindEnumMember', { link = 'HalfspaceTurquoise' })
	highlight(0, 'CmpItemKindEvent', { link = 'HalfspaceViolet' })
	highlight(0, 'CmpItemKindField', { link = 'HalfspaceTurquoise' })
	highlight(0, 'CmpItemKindFile', { link = 'HalfspaceBlue' })
	highlight(0, 'CmpItemKindFolder', { link = 'HalfspaceBlue' })
	highlight(0, 'CmpItemKindFunction', { link = 'HalfspaceSky' })
	highlight(0, 'CmpItemKindInterface', { link = 'HalfspaceEmerald' })
	highlight(0, 'CmpItemKindKeyword', { link = 'HalfspaceViolet' })
	highlight(0, 'CmpItemKindMethod', { link = 'HalfspaceSky' })
	highlight(0, 'CmpItemKindModule', { link = 'HalfspaceEmerald' })
	highlight(0, 'CmpItemKindOperator', { link = 'HalfspaceViolet' })
	highlight(0, 'CmpItemKindProperty', { link = 'HalfspaceTurquoise' })
	highlight(0, 'CmpItemKindReference', { link = 'HalfspaceTurquoise' })
	highlight(0, 'CmpItemKindSnippet', { link = 'HalfspaceGreen' })
	highlight(0, 'CmpItemKindStruct', { link = 'HalfspaceEmerald' })
	highlight(0, 'CmpItemKindText', { fg = black })
	highlight(0, 'CmpItemKindTypeParameter', { link = 'HalfspaceEmerald' })
	highlight(0, 'CmpItemKindUnit', { link = 'HalfspaceTurquoise' })
	highlight(0, 'CmpItemKindValue', { link = 'HalfspaceTurquoise' })
	highlight(0, 'CmpItemKindVariable', { link = 'HalfspaceTurquoise' })
	highlight(0, 'CmpItemMenu', { fg = black })

	-- aerial
	highlight(0, 'AerialGuide', { fg = hsgray06 })
	highlight(0, 'AerialArrayIcon', { link = '@constant' })
	highlight(0, 'AerialBooleanIcon', { link = '@boolean' })
	highlight(0, 'AerialClassIcon', { link = '@type' })
	highlight(0, 'AerialEnumIcon', { link = '@type' })
	highlight(0, 'AerialEnumMembeIcon', { link = '@field' })
	highlight(0, 'AerialEventIcon', { link = '@type' })
	highlight(0, 'AerialFieldIcon', { link = '@field' })
	highlight(0, 'AerialFileIcon', { link = '@text' })
	highlight(0, 'AerialFunctionIcon', { link = '@function' })
	highlight(0, 'AerialInterfaceIcon', { link = '@type' })
	highlight(0, 'AerialKeyIcon', { link = '@type' })
	highlight(0, 'AerialMethodIcon', { link = '@method' })
	highlight(0, 'AerialModuleIcon', { link = '@namespace' })
	highlight(0, 'AerialNamespaceIcon', { link = '@namespace' })
	highlight(0, 'AerialNullIcon', { link = '@type' })
	highlight(0, 'AerialNumberIcon', { link = '@number' })
	highlight(0, 'AerialObjectIcon', { link = '@type' })
	highlight(0, 'AerialOperatorIcon', { link = '@operator' })
	highlight(0, 'AerialPackageIcon', { link = '@namespace' })
	highlight(0, 'AerialPropertyIcon', { link = '@method' })
	highlight(0, 'AerialStringIcon', { link = '@string' })
	highlight(0, 'AerialStructIcon', { link = '@type' })
	highlight(0, 'AerialTypeParameterIcon', { link = '@parameter' })
	highlight(0, 'AerialVariableIcon', { link = '@constant' })

	-- nvim-dap-ui
	highlight(0, 'DapUIType', { link = 'Type' })
	highlight(0, 'DapUIScope', { fg = blue })
	highlight(0, 'DapUINormal', { fg = black })
	highlight(0, 'DapUINormalNC', { fg = black })
	highlight(0, 'DapUIPlayPause', { fg = lime, bg = hsgray04 })
	highlight(0, 'DapUIPlayPauseNC', { fg = lime, bg = hsgray04 })
	highlight(0, 'DapUIStepOver', { fg = blue, bg = hsgray04 })
	highlight(0, 'DapUIStepOverNC', { fg = blue, bg = hsgray04 })
	highlight(0, 'DapUIStepInto', { fg = blue, bg = hsgray04 })
	highlight(0, 'DapUIStepIntoNC', { fg = blue, bg = hsgray04 })
	highlight(0, 'DapUIStepOut', { fg = blue, bg = hsgray04 })
	highlight(0, 'DapUIStepOutNC', { fg = blue, bg = hsgray04 })
	highlight(0, 'DapUIStepBack', { fg = blue, bg = hsgray04 })
	highlight(0, 'DapUIStepBackNC', { fg = blue, bg = hsgray04 })
	highlight(0, 'DapUIRestart', { fg = red, bg = hsgray04 })
	highlight(0, 'DapUIRestartNC', { fg = red, bg = hsgray04 })
	highlight(0, 'DapUIStop', { fg = red, bg = hsgray04 })
	highlight(0, 'DapUIStopNC', { fg = red, bg = hsgray04 })
	highlight(0, 'DapUIUnavailable', { fg = hsgray07, bg = hsgray04 })
	highlight(0, 'DapUIUnavailableNC', { fg = hsgray07, bg = hsgray04 })
	highlight(0, 'DapUIDecoration', { fg = hsgray04 })
	highlight(0, 'DapUIModifiedValue', { fg = violet })
	-- highlight(0, 'DapUIBreakpointsline', { fg = "#626262" })
	highlight(0, 'DapUIBreakpointscurrentline', { fg = lime, bold = true })
	highlight(0, 'DapUIBreakpointsPath', { fg = blue })
	highlight(0, 'DapUIStoppedThread', { fg = purple })
	highlight(0, 'DapUISource', { fg = blue })
	highlight(0, 'DapUILineNumber', { fg = orange })
	highlight(0, 'DapUIWatchesEmpty', { fg = red })
	highlight(0, 'DapUIThread', { fg = lime })

	-- Indent Blankline v3 & later
	highlight(0, 'IblIndent', { link = 'Halfspacegray13', })
	highlight(0, 'IblScope', { link = 'Halfspacegray13', })
	highlight(0, 'IblWhitespace', { link = 'Whitespace', })
	-- Indent Blankline plugin v2 & earlier
	highlight(0, 'IndentBlanklineChar', { link = 'IblIndent' })
	highlight(0, 'IndentBlanklineSpaceChar', { link = 'IblWhitespace' })
	highlight(0, 'IndentBlanklineSpaceCharBlankline', { link = 'IblWhitespace' })

	-- nvim-treesitter-context
	highlight(0, 'TreesitterContext', { underdashed = true, sp = hsgray14 })
	highlight(0, 'TreesitterContextLineNumber', { underdashed = true, sp = hsgray14, fg = hsgray04 })

	-- -- Mini.nvim
	-- highlight(0, "MiniCompletionActiveParameter", { link = "HalfspaceVisual" })
	-- highlight(0, "MiniCursorword", { link = "HalfspaceUnderline" })
	-- highlight(0, "MiniCursorwordCurrent", { link = "HalfspaceUnderline" })
	-- highlight(0, "MiniIndentscopePrefix", { link = "HalfspaceNoCombine" })
	-- highlight(0, "MiniIndentscopeSymbol", { link = "HalfspaceBlack" })
	-- highlight(0, "MiniJump", { link = "SpellRare" })
	-- highlight(0, "MiniStarterCurrent", { link = "HalfspaceNoCombine" })
	-- highlight(0, "MiniStarterFooter", { link = "Title" })
	-- highlight(0, "MiniStarterHeader", { link = "HalfspaceViolet" })
	-- highlight(0, "MiniStarterInactive", { link = "Comment" })
	-- highlight(0, "MiniStarterItem", { link = "Normal" })
	-- highlight(0, "MiniStarterItemBullet", { link = "Delimiter" })
	-- highlight(0, "MiniStarterItemPrefix", { link = "HalfspaceYellow" })
	-- highlight(0, "MiniStarterQuery", { link = "HalfspaceSky" })
	-- highlight(0, "MiniStarterSection", { link = "HalfspaceCrimson" })
	-- highlight(0, "MiniStatuslineModeCommand", { fg = black, bg = yellow_fill })
	-- highlight(0, "MiniStatuslineModeInsert", { fg = black, bg = turquoise_fill })
	-- highlight(0, "MiniStatuslineModeNormal", { fg = black, bg = hsgray04 })
	-- highlight(0, "MiniStatuslineModeOther", { fg = black, bg = hsgray04 })
	-- highlight(0, "MiniStatuslineModeReplace", { fg = black, bg = orange_fill })
	-- highlight(0, "MiniStatuslineModeVisual", { fg = black, bg = purple_fill })
	-- highlight(0, "MiniSurround", { link = "IncSearch" })
	-- highlight(0, "MiniTablineCurrent", { fg = black, bg = hsgray03 })
	-- highlight(0, "MiniTablineFill", { link = "TabLineFill" })
	-- highlight(0, "MiniTablineModifiedCurrent", { fg = yellow_fill, bg = hsgray12 })
	-- highlight(0, "MiniTablineModifiedVisible", { fg = lime_fill, bg = hsgray12 })
	-- highlight(0, "MiniTablineTabpagesection", { fg = black, bg = hsgray04 })
	-- highlight(0, "MiniTablineVisible", { fg = hsgray00, bg = hsgray12 })
	-- highlight(0, "MiniTestEmphasis", { link = "HalfspaceUnderline" })
	-- highlight(0, "MiniTestFail", { link = "HalfspaceRed" })
	-- highlight(0, "MiniTestPass", { link = "HalfspaceGreen" })
	-- highlight(0, "MiniTrailspace", { fg = black, bg = crimson_fill })
	-- highlight(0, "MiniJump2dSpot", { fg = yellow, underline = true, nocombine = true })
	-- highlight(0, "MiniStatuslineDevinfo", { bg = hsgray04, fg = white })
	-- highlight(0, "MiniStatuslineFileinfo", { bg = hsgray04, fg = white })
	-- highlight(0, "MiniStatuslineFilename", { bg = hsgray11, fg = black })
	-- highlight(0, "MiniStatuslineInactive", { bg = hsgray11, fg = black })
	-- highlight(0, "MiniTablineHidden", { bg = hsgray11, fg = hsgray08 })
	-- highlight(0, "MiniTablineModifiedHidden", { bg = hsgray11, fg = yellow })

	-- dashboard-nvim
	highlight(0, 'DashboardCenter', { fg = violet })
	highlight(0, 'DashboardHeader', { fg = coral })
	highlight(0, 'DashboardFooter', { fg = black })
	highlight(0, 'DashboardShortCut', { fg = turquoise })

	-- alpha-nvim
	highlight(0, 'AlphaHeader', { fg = coral })
	highlight(0, 'AlphaFooter', { fg = black })

	-- nvim-notify
	highlight(0, 'NotifyERRORBorder', { fg = red })
	highlight(0, 'NotifyWARNBorder', { fg = yellow })
	highlight(0, 'NotifyINFOBorder', { link = 'FloatBorder' })
	highlight(0, 'NotifyDEBUGBorder', { link = 'FloatBorder' })
	highlight(0, 'NotifyTRACEBorder', { link = 'FloatBorder' })
	highlight(0, 'NotifyERRORIcon', { link = 'HalfspaceRed' })
	highlight(0, 'NotifyWARNIcon', { link = 'HalfspaceYellow' })
	highlight(0, 'NotifyINFOIcon', { link = 'HalfspaceBlue' })
	highlight(0, 'NotifyDEBUGIcon', { link = 'Halfspacegray04' })
	highlight(0, 'NotifyTRACEIcon', { link = 'HalfspacePurple' })
	highlight(0, 'NotifyERRORTitle', { link = 'HalfspaceRed' })
	highlight(0, 'NotifyWARNTitle', { link = 'HalfspaceYellow' })
	highlight(0, 'NotifyINFOTitle', { link = 'HalfspaceBlue' })
	highlight(0, 'NotifyDEBUGTitle', { link = 'Halfspacegray04' })
	highlight(0, 'NotifyTRACETitle', { link = 'HalfspacePurple' })

	-- lazy.nvim
	highlight(0, 'LazyCommit', { link = 'HalfspaceEmerald' })
	highlight(0, 'LazyCommitType', { link = 'HalfspaceViolet' })
	highlight(0, 'LazyH1', { fg = black, bg = purple_fill })
	highlight(0, 'LazyDir', { link = 'Directory' })
	highlight(0, 'LazyUrl', { link = 'Directory' })
	highlight(0, 'LazyProgressDone', { link = 'HalfspaceBlue' })
	highlight(0, 'LazyReasonCmd', { link = 'HalfspaceGreen' })
	highlight(0, 'LazyReasonStart', { link = 'HalfspaceBlue' })
	highlight(0, 'LazyReasonPlugin', { link = 'HalfspaceCoral' })
	highlight(0, 'LazyReasonRuntime', { link = 'HalfspaceViolet' })
	highlight(0, 'LazyReasonRequire', { link = 'HalfspaceYellow' })
	highlight(0, 'LazySpecial', { fg = turquoise })
	highlight(0, 'LazyLocal', { fg = crimson })
	highlight(0, 'LazyButton', { bg = hsgray03, fg = black })
	highlight(0, 'LazyButtonActive', { bg = turquoise_bg, fg = black })
	highlight(0, 'LazyNormal', { link = 'NormalFloat' })

	-- mason.nvim
	highlight(0, 'MasonError', { link = 'HalfspaceRed' })
	highlight(0, 'MasonHeader', { fg = black, bg = coral_fill })
	highlight(0, 'MasonHeaderSecondary', { fg = black, bg = green_fill })
	highlight(0, 'MasonHeading', { fg = black })
	highlight(0, 'MasonHighlight', { link = 'HalfspaceSky' })
	highlight(0, 'MasonHighlightBlock', { fg = black, bg = blue_fill })
	highlight(0, 'MasonHighlightBlockBold', { fg = hsgray00, bg = hsgray12 })
	highlight(0, 'MasonHighlightBlockBoldSecondary', { fg = black, bg = green_fill })
	highlight(0, 'MasonHighlightBlockSecondary', { fg = black, bg = green_bg })
	highlight(0, 'MasonHighlightSecondary', { fg = green, bg = green_bg })
	highlight(0, 'MasonLink', { link = 'HalfspacePurple' })
	highlight(0, 'MasonMuted', { link = 'Comment' })
	highlight(0, 'MasonMutedBlock', { bg = hsgray03, fg = black })
	highlight(0, 'MasonMutedBlockBold', { bg = hsgray03, fg = black })

	-- linefly
	highlight(0, 'LineflyNormal', { fg = hsgray00, bg = hsgray12 })
	highlight(0, 'LineflyInsert', { fg = emerald_fill, bg = hsgray12 })
	highlight(0, 'LineflyVisual', { fg = purple_fill, bg = hsgray12 })
	highlight(0, 'LineflyCommand', { fg = yellow_fill, bg = hsgray12 })
	highlight(0, 'LineflyReplace', { fg = crimson_fill, bg = hsgray12 })

	-- -- nvim-navic
	-- highlight(0, "NavicText", { bg = hsgray11, fg = black })
	-- highlight(0, "NavicSeparator", { bg = hsgray11, fg = white })
	-- highlight(0, "NavicIconsOperator", { bg = hsgray11, fg = cranberry })
	-- highlight(0, "NavicIconsBoolean", { link = "NavicIconsOperator" })
	-- highlight(0, "NavicIconsClass", { bg = hsgray11, fg = emerald })
	-- highlight(0, "NavicIconsConstant", { bg = hsgray11, fg = orange })
	-- highlight(0, "NavicIconsConstructor", { bg = hsgray11, fg = sky })
	-- highlight(0, "NavicIconsEnum", { bg = hsgray11, fg = violet })
	-- highlight(0, "NavicIconsEnumMember", { bg = hsgray11, fg = turquoise })
	-- highlight(0, "NavicIconsEvent", { link = "NavicIconsConstant" })
	-- highlight(0, "NavicIconsField", { link = "NavicIconsEnumMember" })
	-- highlight(0, "NavicIconsFile", { bg = hsgray11, fg = blue })
	-- highlight(0, "NavicIconsFunction", { link = "NavicIconsConstructor" })
	-- highlight(0, "NavicIconsInterface", { link = "NavicIconsEnum" })
	-- highlight(0, "NavicIconsKey", { link = "NavicIconsEnumMember" })
	-- highlight(0, "NavicIconsMethod", { link = "NavicIconsConstructor" })
	-- highlight(0, "NavicIconsModule", { link = "NavicIconsEnumMember" })
	-- highlight(0, "NavicIconsNamespace", { link = "NavicIconsEnumMember" })
	-- highlight(0, "NavicIconsNull", { bg = hsgray11, fg = green })
	-- highlight(0, "NavicIconsNumber", { link = "NavicIconsConstant" })
	-- highlight(0, "NavicIconsObject", { link = "NavicIconsEnumMember" })
	-- highlight(0, "NavicIconsPackage", { link = "NavicIconsEnumMember" })
	-- highlight(0, "NavicIconsProperty", { link = "NavicIconsEnumMember" })
	-- highlight(0, "NavicIconsString", { bg = hsgray11, fg = khaki })
	-- highlight(0, "NavicIconsStruct", { link = "NavicIconsClass" })
	-- highlight(0, "NavicIconsTypeParameter", { link = "NavicIconsEnumMember" })
	-- highlight(0, "NavicIconsVariable", { link = "NavicIconsEnumMember" })

	-- Rainbow Delimiters
	highlight(0, 'RainbowDelimiterRed', { link = 'HalfspaceRed' })
	highlight(0, 'RainbowDelimiterYellow', { link = 'HalfspaceYellow' })
	highlight(0, 'RainbowDelimiterBlue', { link = 'HalfspaceBlue' })
	highlight(0, 'RainbowDelimiterOrange', { link = 'HalfspaceOrange' })
	highlight(0, 'RainbowDelimiterGreen', { link = 'HalfspaceGreen' })
	highlight(0, 'RainbowDelimiterViolet', { link = 'HalfspaceViolet' })
	highlight(0, 'RainbowDelimiterCyan', { link = 'HalfspaceTurquoise' })

	-- -- Neogit
	-- highlight(0, "NeogitBranch", { link = "HalfspaceSky" })
	-- highlight(0, "NeogitDiffAddHighlight", { bg = green_bg})
	-- highlight(0, "NeogitDiffContextHighlight", { bg = hsgray14 })
	-- highlight(0, "NeogitDiffDeleteHighlight", { bg = red_bg, fg = red_fill })
	-- highlight(0, "NeogitHunkHeader", { link = "Pmenu" })
	-- highlight(0, "NeogitHunkHeaderHighlight", { fg = black })
	-- highlight(0, "NeogitRemote", { link = "HalfspacePurple" })

	-- more diff groups that some plugins make use of
	highlight(0, 'Added', { fg = green })
	highlight(0, 'diffAdded', { link = 'HalfspaceGreen' })
	highlight(0, 'Changed', { fg = blue })
	highlight(0, 'diffChanged', { link = 'HalfspaceBlue' })
	highlight(0, 'diffLine', { link = 'HalfspaceBlue' })
	highlight(0, 'Removed', { fg = red })
	highlight(0, 'diffRemoved', { link = 'HalfspaceRed' })
	highlight(0, 'diffSubname', { link = 'HalfspaceBlue' })
	highlight(0, 'diffIndexLine', { link = 'HalfspaceCrimson' })
end

return M
