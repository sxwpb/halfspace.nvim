if has('nvim') && !has('nvim-0.8')
	lua vim.api.nvim_echo({{ "halfspace requires Neovim 0.8 or later.\n", "WarningMsg" }}, false, {})
	finish
endif

" Clear highlights and reset syntax.
highlight clear
if exists('syntax_on')
	syntax reset
endif
let g:colors_name='halfspace'

" Enable terminal true-color support.
set termguicolors

" Set the colors.
lua require("halfspace").style()

" Set background option (at the end for startup performance reasons).
set background=light
