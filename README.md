![header-img](/uploads/c7bd3c5c7ee6d0d3a440dd330fdd0284/hlvsps.jpg)
==========

halfspace is a semi-light neovim color scheme.

Screenshots
-----------

![Screenshot_from_2024-02-12_22-50-10](/uploads/6c3e240392d4e8417a7f2e11910b6f92/screenshot1.png)

![Screenshot_from_2024-02-12_23-01-18](/uploads/93ca0394d8d25727945fab4fdde72caf/screenshot2.png)

![Screenshot_from_2024-02-12_23-31-02](/uploads/0a41f82cd92cf95b48843436440f5a24/screenshot3.png)

Requirements
------------

nvim 0.8 or later and a true-color terminal or GUI.

Installation
------------

The colorscheme can be installed with your preferred plugin manager.

Example with [lazy.nvim](https://github.com/folke/lazy.nvim):

```lua
{ url = 'https://gitlab.com/sxwpb/halfspace.nvim', },
```

Usage
-----

Enable the colorscheme in your lua config like so:

```lua
vim.cmd.colorscheme('halfspace')
```

Set custom colors
-----------------

If a color of this theme does not suit, it is recommended to specify
your custom overrides using an `autocmd` before enabling the colorscheme.

Example:

```lua
-- set up the autocmd to override colors
vim.api.nvim_create_autocmd('ColorScheme', {
  group = vim.api.nvim_create_augroup('halfspace_colors_custom', {}),
  pattern = 'halfspace',
  callback = function()
    local highlight = vim.api.nvim_set_hl

    -- specify your preferred overrides
    highlight(0, 'Function', { fg = '#ff0000', bold = true })
    highlight(0, 'FloatBorder', { fg = '#0000ff', })
  end,
})

-- enable the colorscheme
vim.cmd.colorscheme('halfspace')
```

Leave a donation :cherry_blossom:
------------------------

<https://donate.stripe.com/fZe8zh2wSgH17IIcMM>

Credits
-------

This theme was started and created using [moonfly](https://github.com/bluz71/vim-moonfly-colors)'s setup files.
I use moonfly as my preferred dark theme, I think it's really nicely designed.
Have a look at it.

I used oklch.com to help pick some colors.

License
-------

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)
